const fs = require('fs');
const path = require('path');
const express = require('express');
const app = express();
const httpolyglot = require('httpolyglot');
const https = require('https');
const http = require('http');

//////// CONFIGURATION ///////////

// insert your own ssl certificate and keys

const options = {
    key: fs.readFileSync('ssl/key.pem'),
    cert: fs.readFileSync('ssl/cert.pem'),
};

const port = process.env.PORT || 3000;

////////////////////////////

require('./routes')(app);

const server = https.createServer(options, app).listen(port, () => {
    console.log(`listening on port ${port}`);
});

// const server = http.createServer(app).listen(port, () => {
//     console.log(`listening on port ${port}`);
// });
const io = require('socket.io')(server);
require('./socketController')(io);
