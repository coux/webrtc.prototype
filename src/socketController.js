peers = {};

module.exports = (io) => {
    io.on('connect', (socket) => {
        console.log(`신규 멈버(${socket.id})가 접속 했습니다.`);
        socket.emit('init', socket.id);
        // Initiate the connection process as soon as the client connects

        peers[socket.id] = socket;

        // Asking all other clients to setup the peer connection receiver
        for (let id in peers) {
            if (id === socket.id) continue;
            console.log(`기존 멤버(${id})에게 신규 멤버(${socket.id}) 피어 정보를 보냅니다.`);
            peers[id].emit('initReceive', socket.id);
        }

        /**
         * relay a peerconnection signal to a specific socket
         */
        socket.on('signal', (data) => {
            console.log(`해당 멤버${data.socket_id} 에게 ${data.signal.type} 신호를 보냅니다.`);
            if (!peers[data.socket_id]) return;
            peers[data.socket_id].emit('signal', {
                socket_id: socket.id,
                signal: data.signal,
            });
        });

        /**
         * remove the disconnected peer connection from all other connected clients
         */
        socket.on('disconnect', () => {
            console.log(`모든 멤버로 부터 해당 멤버의(${socket.id}) 소켓 커넥션을 제거 합니다.`);
            socket.broadcast.emit('removePeer', socket.id);
            delete peers[socket.id];
        });

        /**
         * Send message to client to initiate a connection
         * The sender has already setup a peer connection receiver
         */
        socket.on('initSend', (init_socket_id) => {
            console.log(`신규 멤버(${init_socket_id})에게 기존 멤버의(${socket.id}) 피어 정보를 전달 합니다.`);
            peers[init_socket_id].emit('initSend', socket.id);
        });
    });
};
