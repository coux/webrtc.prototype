/**
 * Socket.io socket
 */
let socket;
/**
 * The stream object used to send media
 */
let localStream = null;
/**
 * All peer connections
 */
let peers = {};

// redirect if not https
if (location.href.substr(0, 5) !== 'https') location.href = 'https' + location.href.substr(4, location.href.length - 4);

//////////// CONFIGURATION //////////////////

/**
 * RTCPeerConnection configuration
 */
const configuration = {
    iceServers: [
        {
            urls: 'stun:stun.l.google.com:19302',
        },
        // public turn server from https://gist.github.com/sagivo/3a4b2f2c7ac6e1b5267c2f1f59ac6c6b
        // set your own servers here
        {
            urls: 'turn:192.158.29.39:3478?transport=udp',
            credential: 'JZEOEt2V3Qb0y27GRntt2u2PAYA=',
            username: '28224511:1379330808',
        },
    ],
};

// const removeAllButH264 = (sdp) => {
//     sdp = sdp.replace('a=rtpmap:120 VP8/90000\r\n', '');
//     sdp = sdp.replace('a=rtpmap:121 VP9/90000\r\n', '');
//     sdp = sdp.replace(/m=video ([0-9]+) UDP\/TLS\/RTP\/SAVPF ([0-9 ]*) 120/g, 'm=video $1  UDP/TLS/RTP/SAVPF $2');
//     sdp = sdp.replace(/m=video ([0-9]+) UDP\/TLS\/RTP\/SAVPF 120([0-9 ]*)/g, 'm=video $1 UDP/TLS/RTP/SAVPF$2');
//     sdp = sdp.replace(/m=video ([0-9]+) UDP\/TLS\/RTP\/SAVPF ([0-9 ]*) 121/g, 'm=video $1  UDP/TLS/RTP/SAVPF $2');
//     sdp = sdp.replace(/m=video ([0-9]+) UDP\/TLS\/RTP\/SAVPF 121([0-9 ]*)/g, 'm=video $1 UDP/TLS/RTP/SAVPF$2');
//     sdp = sdp.replace('a=rtcp-fb:120 goog-remb\r\n', '');
//     sdp = sdp.replace('a=rtcp-fb:120 nack\r\n', '');
//     sdp = sdp.replace('a=rtcp-fb:120 nack pli\r\n', '');
//     sdp = sdp.replace('a=rtcp-fb:120 ccm fir\r\n', '');
//     sdp = sdp.replace('a=rtcp-fb:121 goog-remb\r\n', '');
//     sdp = sdp.replace('a=rtcp-fb:121 nack\r\n', '');
//     sdp = sdp.replace('a=rtcp-fb:121 nack pli\r\n', '');
//     sdp = sdp.replace('a=rtcp-fb:121 ccm fir\r\n', '');
//     return sdp;
// };

/**
 * UserMedia constraints
 */
let constraints = {
    // audio: true,
    video: { width: { min: 640 }, height: { min: 480 } },
};

/////////////////////////////////////////////////////////

constraints.video.facingMode = {
    ideal: 'user',
};

// enabling the camera at startup
navigator.mediaDevices
    .getUserMedia(constraints)
    .then((stream) => {
        console.log('미디어 스트림 확인...');
        localVideo.srcObject = stream;
        localVideo.setAttribute = 'video/mp4; codecs="avc1.4d002a"';
        localStream = stream;
        init();
    })
    .catch((e) => alert(`카메라나 마이크를 확인 해주세요. ${e.name}`));

/**
 * initialize the socket connections
 */
function init() {
    socket = io();

    socket.on('init', (socket_id) => {
        let label = document.createElement('li');
        label.innerHTML = socket_id;
        localVideo.parentNode.getElementsByTagName('ul')[0].appendChild(label);
        // localVideo.parentNode.getElementsByTagName('h1')[0].innerText = socket_id;
        localVideo.parentNode.id = socket_id;
    });

    socket.on('initReceive', (socket_id) => {
        console.log(`게스트(${socket_id})를 피어 커넥션에 추가 합니다.`);
        addPeer(socket_id, false);
        socket.emit('initSend', socket_id);
    });

    socket.on('initSend', (socket_id) => {
        console.log(`호스트(${socket_id})를 피어 커넥션에 추가 합니다.`);
        addPeer(socket_id, true);
    });

    socket.on('removePeer', (socket_id) => {
        console.log(`피어 커넥션(${socket_id})을 제거 합니다.`);
        removePeer(socket_id);
    });

    socket.on('disconnect', () => {
        console.log('소켓 통신을 종료 합니다');
        for (let socket_id in peers) {
            console.log(`피어 커넥션(${socket_id})을 제거 합니다.`);
            removePeer(socket_id);
        }
    });

    socket.on('signal', (data) => {
        console.log('신호');
        if (peers[data.socket_id]) {
            peers[data.socket_id].signal(data.signal);
        }
    });
}

/**
 * Remove a peer with given socket_id.
 * Removes the video element and deletes the connection
 * @param {String} socket_id
 */
function removePeer(socket_id) {
    let container = document.getElementById(socket_id);
    if (container) {
        let videoEl = container.getElementsByTagName('video')[0];
        const tracks = videoEl.srcObject.getTracks();
        tracks.forEach(function (track) {
            track.stop();
        });

        videoEl.srcObject = null;
        videoEl.parentNode.removeChild(videoEl);
        container.parentNode.removeChild(container);
    }

    if (peers[socket_id]) peers[socket_id].destroy();
    delete peers[socket_id];

    let ul = localVideo.parentNode.getElementsByTagName('ul')[0];
    ul.innerHTML = '';
    let label = document.createElement('li');
    label.innerHTML = localVideo.parentNode.id;
    ul.appendChild(label);
    Object.keys(peers).forEach((peer) => {
        let label = document.createElement('li');
        label.innerHTML = peer;
        ul.appendChild(label);
    });
}

/**
 * Creates a new peer connection and sets the event listeners
 * @param {String} socket_id
 *                 ID of the peer
 * @param {Boolean} am_initiator
 *                  Set to true if the peer initiates the connection process.
 *                  Set to false if the peer receives the connection.
 */
function addPeer(socket_id, am_initiator) {
    const label = document.createElement('li');
    label.innerHTML = socket_id;
    localVideo.parentNode.getElementsByTagName('ul')[0].appendChild(label);

    peers[socket_id] = new SimplePeer({
        initiator: am_initiator,
        stream: localStream,
        config: configuration,
    });

    peers[socket_id].on('signal', (data) => {
        // if (data.type === 'offer') {
        //     data.sdp = removeAllButH264(data.sdp);
        // }

        socket.emit('signal', {
            signal: data,
            socket_id: socket_id,
        });
    });

    peers[socket_id].on('stream', (stream) => {
        let container = document.createElement('div');
        let labels = document.createElement('ul');
        let label = document.createElement('li');
        label.innerHTML = socket_id;
        labels.appendChild(label);
        container.appendChild(labels);
        videos.appendChild(container);
        let newVid = document.createElement('video');
        if ('srcObject' in localVideo) {
            newVid.srcObject = stream;
        } else {
            newVid.src = window.URL.createObjectURL(stream); // for older browsers
        }
        newVid.srcObject = stream;
        newVid.setAttribute('playsinline', true);
        newVid.playsinline = false;
        newVid.autoplay = true;
        newVid.className = 'video';
        newVid.onclick = () => openPictureMode(newVid);
        newVid.ontouchstart = (e) => openPictureMode(newVid);
        container.appendChild(newVid);
        container.className = 'wrap';
        container.id = socket_id;
    });

    peers[socket_id].on('error', (e) => console.log(e));
}

/**
 * Opens an element in Picture-in-Picture mode
 * @param {HTMLVideoElement} el video element to put in pip mode
 */
function openPictureMode(el) {
    console.log('opening pip');
    el.requestPictureInPicture();
}

/**
 * Switches the camera between user and environment. It will just enable the camera 2 cameras not supported.
 */
function switchMedia() {
    if (constraints.video.facingMode.ideal === 'user') {
        constraints.video.facingMode.ideal = 'environment';
    } else {
        constraints.video.facingMode.ideal = 'user';
    }

    const tracks = localStream.getTracks();

    tracks.forEach(function (track) {
        track.stop();
    });

    localVideo.srcObject = null;
    navigator.mediaDevices.getUserMedia(constraints).then((stream) => {
        for (let socket_id in peers) {
            for (let index in peers[socket_id].streams[0].getTracks()) {
                for (let index2 in stream.getTracks()) {
                    if (peers[socket_id].streams[0].getTracks()[index].kind === stream.getTracks()[index2].kind) {
                        peers[socket_id].replaceTrack(peers[socket_id].streams[0].getTracks()[index], stream.getTracks()[index2], peers[socket_id].streams[0]);
                        break;
                    }
                }
            }
        }

        localStream = stream;
        localVideo.srcObject = stream;
        updateButtons();
    });
}

/**
 * Enable screen share
 */
function setScreen() {
    navigator.mediaDevices.getDisplayMedia().then((stream) => {
        for (let socket_id in peers) {
            for (let index in peers[socket_id].streams[0].getTracks()) {
                for (let index2 in stream.getTracks()) {
                    if (peers[socket_id].streams[0].getTracks()[index].kind === stream.getTracks()[index2].kind) {
                        peers[socket_id].replaceTrack(peers[socket_id].streams[0].getTracks()[index], stream.getTracks()[index2], peers[socket_id].streams[0]);
                        break;
                    }
                }
            }
        }
        localStream = stream;
        localVideo.srcObject = localStream;
        socket.emit('removeUpdatePeer', '');
    });
    updateButtons();
}

/**
 * Disables and removes the local stream and all the connections to other peers.
 */
function removeLocalStream() {
    if (localStream) {
        const tracks = localStream.getTracks();

        tracks.forEach(function (track) {
            track.stop();
        });

        localVideo.srcObject = null;
    }

    for (let socket_id in peers) {
        removePeer(socket_id);
    }
}

/**
 * Enable/disable microphone
 */
function toggleMute() {
    for (let index in localStream.getAudioTracks()) {
        localStream.getAudioTracks()[index].enabled = !localStream.getAudioTracks()[index].enabled;
        muteButton.innerText = localStream.getAudioTracks()[index].enabled ? 'Unmuted' : 'Muted';
    }
}
/**
 * Enable/disable video
 */
function toggleVid() {
    for (let index in localStream.getVideoTracks()) {
        localStream.getVideoTracks()[index].enabled = !localStream.getVideoTracks()[index].enabled;
        vidButton.innerText = localStream.getVideoTracks()[index].enabled ? 'Video Enabled' : 'Video Disabled';
    }
}

/**
 * updating text of buttons
 */
function updateButtons() {
    for (let index in localStream.getVideoTracks()) {
        vidButton.innerText = localStream.getVideoTracks()[index].enabled ? 'Video Enabled' : 'Video Disabled';
    }
    for (let index in localStream.getAudioTracks()) {
        muteButton.innerText = localStream.getAudioTracks()[index].enabled ? 'Unmuted' : 'Muted';
    }
}
